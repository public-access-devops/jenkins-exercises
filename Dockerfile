FROM node:21-alpine3.18

RUN mkdir -p /usr/app
RUN mkdir -p /usr/app/images
COPY app/* /usr/app/
COPY app/images/* /usr/app/images

WORKDIR /usr/app
EXPOSE 3000

RUN npm install
CMD ["node", "server.js"]
